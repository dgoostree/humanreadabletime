package com.goostree.codewars;

public class HumanReadableTime {
	public static String makeReadable(int seconds) {
	    
		int secondsDisplay = seconds % 60;
		
		int minuteCount = seconds / 60;
		int minuteDisplay = minuteCount % 60;
		
		int hourDisplay = minuteCount / 60;
		
	    return formatString(hourDisplay, minuteDisplay, secondsDisplay);
	}
	
	private static String formatString(int hours, int minutes, int seconds) {
		return  String.format("%02d:%02d:%02d", hours, minutes, seconds);
	}
}
